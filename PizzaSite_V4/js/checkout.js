var myCart = {}; 
myCart = {
    "name": "",
    "address1": "",
    "zip": "",
    "phone": "",
    "items": [
        {
            "type": "",
            "name": "",
            "size": ""
        }
    ]
};

$(document).ready( function () {
    $('button.setting').click(button_clicked);
    $('#submit').click(send_string);
});

function button_clicked() {
    var $this = $(this);
    var name = $this.data('name');
    var type = $this.data('type');
    var size = $this.data('size');
    myCart.items.push(name);
    myCart.items.push(type);
    myCart.items.push(size);
    var cookie_string = JSON.stringify(myCart);
    document.cookie = 'cookiekey=' +cookie_string + '; max-age=3600; path=/; domain=';
}

function send_string() {
    var json = JSON.stringify(myCart);
    $('#hide').setAttribute('value', json);
}
