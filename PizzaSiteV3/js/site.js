$(document).ready( function () {
    getElementsMeat();
    renderMeat();
    getElementsVeg();
    renderVeg();
    getElementsDrinks();
    renderDrinks();
});

function getElementsMeat() {
    com.dawgpizza.menu.template = $('#meat-template').html();
    com.dawgpizza.menu.$container = $('section.meat');
}

function renderMeat() {
    var templated_data = _.template(com.dawgpizza.menu.template, com.dawgpizza.menu);
    com.dawgpizza.menu.$container.html(templated_data);
}

function getElementsVeg() {
    com.dawgpizza.menu.templateVeg = $('#veg-template').html();
    com.dawgpizza.menu.$containerVeg = $('section.veg');
}

function renderVeg() {
    var templated_dataVeg = _.template(com.dawgpizza.menu.templateVeg, com.dawgpizza.menu);
    com.dawgpizza.menu.$containerVeg.html(templated_dataVeg);
}

function getElementsDrinks() {
    com.dawgpizza.menu.templateDrinks = $('#extras-template').html();
    com.dawgpizza.menu.$containerDrinks = $('section#extras');
}

function renderDrinks() {
    var templated_dataDrinks = _.template(com.dawgpizza.menu.templateDrinks, com.dawgpizza.menu);
    com.dawgpizza.menu.$containerDrinks.html(templated_dataDrinks);
}

function getElementsDesserts() {
    com.dawgpizza.menu.templateDesserts = $('#extras-template').html();
    com.dawgpizza.menu.$containerDesserts = $('section#extras');
}

function renderDesserts() {
    var templated_dataDesserts = _.template(com.dawgpizza.menu.templateDesserts, com.dawgpizza.menu);
    com.dawgpizza.menu.$containerDesserts.html(templated_dataDesserts);
}














